from mcpi.minecraft import Minecraft
from mcpi import block	  

#mc = Minecraft.create() create an mc object with 
ip = "127.0.0.1"
mc = Minecraft.create(ip, 4711)                                        
x, y, z = mc.player.getPos()  
zz = z + 5
x,y,z = 0,0,0
#base
mc.setBlocks(x-7,y, zz-10, x+7, y+10, zz+10,57) # STONE_BRICK 98         
mc.setBlocks(x-6,y+1, zz-9, x+6, y+11, zz+9,  0) # AIR 0   
#doors
mc.setBlocks(x-8,y+1, zz-1, x+8, y+5, zz+1,  0) # AIR 0      
# windows
mc.setBlocks(x-2,y+2, zz-10, x+2, y+4, zz+10,  0) # AIR 0   
#roof
mc.setBlocks(x-8,y+11, zz-11, x+7, y+11, zz+11,57) # STONE_BRICK 98 
#foundation
mc.setBlocks(x-7,y, zz-10, x+7, y, zz+10,89)
#move player
mc.player.setPos(x,y+2,zz)
  
#API Blocks
#====================
#	AIR                   0
#	STONE                 1
#	GRASS                 2
#	DIRT                  3
#	COBBLESTONE           4
#	WOOD_PLANKS           5
#	SAPLING               6
#	BEDROCK               7
#	WATER_FLOWING         8
#	WATER                 8
#	WATER_STATIONARY      9
#	LAVA_FLOWING         10
#	LAVA                 10
#	LAVA_STATIONARY      11
#	SAND                 12
#	GRAVEL               13
#	GOLD_ORE             14
#	IRON_ORE             15
#	COAL_ORE             16
#	WOOD                 17
#	LEAVES               18
#	GLASS                20
#	LAPIS_LAZULI_ORE     21
#	LAPIS_LAZULI_BLOCK   22
#	SANDSTONE            24
#	BED                  26
#	COBWEB               30
#	GRASS_TALL           31
#	WOOL                 35
#	FLOWER_YELLOW        37
#	FLOWER_CYAN          38
#	MUSHROOM_BROWN       39
#	MUSHROOM_RED         40
#	GOLD_BLOCK           41
#	IRON_BLOCK           42
#	STONE_SLAB_DOUBLE    43
#	STONE_SLAB           44
#	BRICK_BLOCK          45
#	TNT                  46
#	BOOKSHELF            47
#	MOSS_STONE           48
#	OBSIDIAN             49
#	TORCH                50
#	FIRE                 51
#	STAIRS_WOOD          53
#	CHEST                54
#	DIAMOND_ORE          56
#	DIAMOND_BLOCK        57
#	CRAFTING_TABLE       58
#	FARMLAND             60
#	FURNACE_INACTIVE     61
#	FURNACE_ACTIVE       62
#	DOOR_WOOD            64
#	LADDER               65
#	STAIRS_COBBLESTONE   67
#	DOOR_IRON            71
#	REDSTONE_ORE         73
#	SNOW                 78
#	ICE                  79
#	SNOW_BLOCK           80
#	CACTUS               81
#	CLAY                 82
#	SUGAR_CANE           83
#	FENCE                85
#	GLOWSTONE_BLOCK      89
#	BEDROCK_INVISIBLE    95
#	STONE_BRICK          98
#	GLASS_PANE          102
#	MELON               103
#	FENCE_GATE          107
#	GLOWING_OBSIDIAN    246
#	NETHER_REACTOR_CORE 247
